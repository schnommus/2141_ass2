import sys
import colorsys

NUM_INPUTS = 4
NUM_OUTPUTS = 1

if( len(sys.argv) < 2 ):
    print("Usage: <input csv>, creates 'state_graph_out.gv'")
    sys.exit(1)

file = open(sys.argv[1]).readlines()

# Ignore title lines
titles = file[1].split(',')
lines = file[2:]

print("digraph G {")
print('size="4,4"')

for line in lines:
    fields = line.split(',')
    label = fields[0]
    stateId = fields[1].upper()

    print(stateId + '[label=< <B>' + stateId + "</B><br/><i>" + label + '</i> >];')

    stateChanges = []
    for stateChange in zip(range(1,NUM_INPUTS+1), fields[2:]):

        # Ignore don't-care edges
        if( stateChange[1].upper() == "X" ):
            continue

        # Pick a colour
        hue = float(stateChange[0]) / float(NUM_INPUTS+1);
        hue = '{0:.{1}f}'.format(hue,2)
        print('edge [color="{} 1 1"];'.format(hue))

        print( stateId + " -> " + stateChange[1].upper() + ' [label="' + titles[1+stateChange[0]].upper() + '"];' )


print("}")
